
package yani.proyecto.proyecto;

import java.awt.Graphics;
import javax.swing.JPanel;

/**
 una lamina actua como capa(ej: photoshop)
 */
public class Lamina extends JPanel{
    
    @Override
    public void paintComponent(Graphics g){
        
        super.paintComponent(g);
        
        //g.drawString("Texto de prueba", 100, 100);    //Imprimir texto en pantalla(texto, posicion en X, posicion en Y)
        
        //dibujar un ovalo
        g.drawOval(100, 150, 200, 100); // 1° localizacion en X,  2° localizacion en Y, 3° ancho, 4° alto (igual para todos los similares)
        
        // tambien existe una clase para dibujar en 2D. (ej: Rectangle2D)

    }
    
}
    

