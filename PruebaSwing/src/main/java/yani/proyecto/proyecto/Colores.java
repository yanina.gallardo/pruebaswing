/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yani.proyecto.proyecto;

import java.awt.Color;
import java.awt.Graphics2D;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 
 * setPaint(color)---> permite establecer el color(solo se puede usar creando la clase 2D)
 * color = (constantes estaticas) BLUE, RED, GREEN, YELLOW, etc.
 *tambien se puede usar el sistema RGB u RGBA(A = transparecia) para construir un color
 * 
 setBackground(color) ---> establece el color de fondo de la lamina
 * setForeground (color) --> establece colores de frente de la lamina
 * 
 */

public class Colores extends JFrame{
   
    public Colores(){
        setSize(600, 700);
        setTitle("ventana colores");
        setVisible(true);
        
        Lamina lamina = new Lamina();
        add(lamina);
        lamina.setBackground(Color.CYAN);
        
        
        
    }
   
}


