/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package yani.proyecto.proyecto;

import yani.proyecto.proyecto.Texto;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JFrame;

/**
 *
 * @author Yani
 */
public class test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {  
                //SWING
          /*
        closeOperation sirve decirle a la consola que hacer al momento de presionar X (cerrar)
        
        DO_NOTHING_ON_CLOSE (defined in WindowConstants): hace que el boton X para cerra sea inefectiva
        HIDE_ON_CLOSE (defined in WindowConstants): esconde la ventana. Se usa para que el programa siga en ejecucion.  
        DISPOSE_ON_CLOSE (defined in WindowConstants):este método se utiliza para cerrar el marco actual. Pero no termina la aplicación.
        EXIT_ON_CLOSE (defined in JFrame): este método se utiliza para cerrar todo el marco y, finalmente, para finalizar la aplicación 
        
        */
          //CLASE IMAGEN
        imagen i = new imagen();

        i.setVisible(true); // hace que una ventana sea visible (true)

        i.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
     
         //CLASE TEXTO
     Texto t = new Texto();
     
     t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     
     //CLASE COLORES
     
     Colores c = new Colores();
     
     c.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }

}
