package yani.proyecto.proyecto;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JFrame;

public class imagen extends JFrame {

    public imagen() {
        setSize(500, 400); // establece la dimension inicial de la ventana (ancho, alto)
        //setBounds(100, 100, 500, 500);
        setTitle("Ventana imagen");
        setIconImage(icono);

        //setExtendedState(Frame.MAXIMIZED_BOTH);
        //setResizable(true);
        Dimension d = tk.getScreenSize();
        int alturaPantalla = d.height;
        int anchoPantalla = d.width;
        setLocation(anchoPantalla / 4, alturaPantalla / 4);
    }

    /*
   TOOLKIT: almacen de multitud de metodos que se comunican con el sistema huesped de ventanas
   Toolkit.getDefaultToolkit() ---> devuelve un tipo Toolkit.. no recibe parametros
   
   getScreenSize() ---> devuelve la resolucion de pantalla de la pc en que se ejecuta. Hereda de Toolkit. se guarda en una variable de tipo Dimension(clase que hay que importar)
      
   EJEMPLO PARA EJECUTAR LA DIEMNSION EN RESOLUCION COMPLETA: tener en cuenta barra de inicio
        Dimension d = tk.getScreenSize();
        int alturaPantalla = d.height;   
        int anchoPantalla = d.width;
        setSize(anchoPantalla, alturaPantalla);
   
    PARA CENTRAR LA VENTANA:
   setSize(500, 500); // dimension deseada de la ventana
   setLocation(anchoPantalla/4, alturaPantalla/4);  //localizacion central en la pantalla, manteniendo el tamaño deseado
   
     */
    // insertar imagen (para guardarla en el proyecto se debe poner la ruta a la carpeta donde va a estar guardada)
    String urlImagen = "C:\\Users\\Yani\\Desktop\\Proyecto swing\\pruebaswing\\PruebaSwing\\imagenes\\dog.png"; //.gif es el tipo que memos pesa

    Toolkit tk = Toolkit.getDefaultToolkit(); // se puede usar para detectar la resolucion de la pantalla y ejecutar en esa dimension. y para agregar imagenes a la aplicacion

    //agregar icono a la ventana
    Image icono = tk.getImage(urlImagen);
    
    public void impreso() {  //imprime en consola la dimension de la pantalla
        Dimension d = tk.getScreenSize();
        System.out.println("dimension = " + d);
    }
    /*
    EJEMPLOS SWING
    
    setLocation(int x,int y)----> cambiar posicion de la ventana [x = horizontal, y = vertical]
    setrBounds(int x,int y, ancho. largo)----> cambiar posicion y dimension de la ventana
    
    setIconImage(Image)---> cambiar icono del marco (a la izquierda)
    
    setTitle (String)---> cambiar titulo de la ventana
    
    setResizable(boolean)---> determinar si se puede o no redimencionar el marco, agrandar por los laterales. si no se escribe por defecto es TRUE
    
    setExtendedState(Frame.MAXIMIZED_BOTH) --> agranda la ventana a su dimension maximizada. Tambien se puede usar MAXIMIZED_VERTICAL U HORIZONTAL
    
     */

}
