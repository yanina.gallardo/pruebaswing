
package yani.proyecto.proyecto;

import java.awt.Graphics;
import javax.swing.JFrame; 
import javax.swing.JPanel;
 
/**
 JPanel es la clase que se utiliza para construir laminas donde poder dibujar y escribir.
 * se debe crear una clase que herede de JPanel.
 * 
 */
public class Texto extends JFrame{
    
    public Texto(){
        setVisible(true);
        setSize(600, 450);
        setLocation(400, 200);
        setTitle("Ventana Texto");
        
        //crear y agregar lamina
        Lamina lamina = new Lamina();
        add(lamina);
    }
}

/*
Crear una clase aca abajo es lo mismo que crearla como nuevo archivo.

La creo asi xq son dos clases conectadas, para que no se pierdan con la explicacion de texto lo hago asi.

paintComponent(Graphics g); //dibuja componentes, botones, imagenes, texto, etc.

clase Graphics sirve para dibujar graficos ensima de una lamina: dibujar figuras, lineas, establecer color y letra, etc

*/


